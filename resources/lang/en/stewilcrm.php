<?php

return [

    /*
    |--------------------------------------------------------------------------
    | STEWILCRM Language Lines
    |--------------------------------------------------------------------------
    |
    | Brice POTE le 29/03/2019
    |
    | t
    |
    */

    'Products_Catalog' => 'Gestion du catalogue',
    'Products' => 'Produits',
    'AdminCRM' => 'Administration du CRM',

];
